# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# User specific aliases and functions
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias term='tabbed -c urxvt256c-ml -embed &'

export PATH=$PATH:/home/nora/bin/:/sbin
