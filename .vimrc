filetype off

" Plugins ''''''''''''''''''''''
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugin manager
Plugin 'VundleVim/Vundle.vim'

" Git plugin
Plugin 'tpope/vim-fugitive'

" Colorschemes
Plugin 'flazz/vim-colorschemes'

" Syntax stuff
" Plugin 'scrooloose/syntastic'

" Quotes and whatnot
Plugin 'tpope/vim-surround'

" Airline status bar
Plugin 'bling/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

" LaTeX
Plugin 'lervag/vimtex'

" CtrlP for tags and what not
Plugin 'ctrlpvim/ctrlp.vim'

" Gutentags for tag files
Plugin 'ludovicchabant/vim-gutentags'

" Delimitmate, autocomplete surrounding tags and what not
Plugin 'raimondi/delimitmate'

call vundle#end()
filetype plugin indent on

""""""""""""""""""""""""""""""""

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Set guten statusline
set statusline+=%{gutentags#statusline('[Generating...]')}

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Annoying rails warning
let g:syntastic_eruby_ruby_quiet_messages =
            \ { 'regex': 'possibly useless use of a variable in void context' }

"end syntastic settings

" Airline statusbar settings

let g:airline_theme='minimalist'
let g:airline_powerline_fonts=1
  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = 'Ξ'

" end airline settings

" vimtex settings
" let g:vimtex_view_method='mupdf'
let g:vimtex_view_general_viewer='mupdf'
let g:vimtex_compiler_latexmk={
    \ 'callback'   : 0,
    \ 'executable' : 'latexmk',
    \ 'options'    : [
    \   '-pdf',
    \   '-interaction=nonstopmode',
    \ ],
    \}

" end vimtex settings

colorscheme 1989
set colorcolumn=80
highlight ColorColumn ctermbg=10

syntax on

set wildmenu

set backspace=indent,eol,start

set autoindent

set number


set noswapfile

set nobackup
set nowritebackup

set noundofile

set nocompatible "disables vi compatibility

set laststatus=2 "always shows status bar

set t_Co=256 "makes sure it uses 256 colors

" tab and file settings
set encoding=utf-8
set shiftwidth=4
set softtabstop=4
set expandtab

autocmd FileType tex setlocal sw=2 sts=2 expandtab
autocmd FileType bib setlocal sw=2 sts=2 expandtab
autocmd FileType ruby setlocal sw=2 sts=2 expandtab
autocmd FileType html setlocal sw=2 sts=2 expandtab
autocmd FileType javascript setlocal sw=2 sts=2 expandtab
autocmd FileType xml setlocal sw=2 sts=2 expandtab
autocmd FileType text setlocal sw=2 sts=2 expandtab
autocmd FileType cweb setlocal sw=4 sts=4 expandtab

" NoWeb syntax settings
au BufRead,BufNewFile *.nw set filetype=noweb
let noweb_backend="tex"
let noweb_language="c"
let noweb_fold_code=0

" keybinds
" leader key for stuff like vimtex
let maplocalleader="\\"

" make all tex files latex
let g:tex_flavor="latex"
